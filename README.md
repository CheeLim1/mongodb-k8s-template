## Step 1 Creating external services configuration

Order of deployment matters create secret first before deploy 

So if doing it manually
> cd into root dir
> kubectl apply -f mongo-secret.yaml
> kubectl get secret

Then reference secret in deployment file
> kubectl apply -f mongo-deployment.yaml
> kubectl get all
> kubectl get pod --watch


## Step 2 Creating internal services config so other pods can talk to this mongodb

Create the mongo-express to run it internally
Create the mongo-configmap basically the value.yaml for mongo-express
Order matters so have to kubectl apply the mongo-configmap before mongo-express.yaml


## Final Step Access Mongo Express from a server
- in order to do that need a service
- will do it inside mongo-express.yaml

To make it accessible need to open a 3rd port which is the nodePort, its the port where the external IP address
will be open. 

after 
kubectl apply -f mongo-configmap.yaml
kubectl apply -f mongo-express.yaml
kubectl get service

#### The flow 
Mongo Express External Service > Mongo Express Pod > MongoDB Internal Service > MongoDB Pod

# Ref
- https://gitlab.com/nanuchi/youtube-tutorial-series/-/tree/master/demo-kubernetes-components

# Notes
> kubectl get service
- Cluster IP only has internal IP address can be accessed internally
- LoadBalance type has external IP address can be accessed web and also internal IP

- selector references the label
- nodePort range is within (30000-32767)

** In minikube need to manually assign external service an IP address **
> kubectl get service
Take the loadbalancer service name
> minikube service <servicename>


# Cuz I'm lazy 
- cd to correct directory
- minikube start
- kubectl apply -f mongo-secret.yaml
- kubectl apply -f mongo-deployment.yaml
- kubectl apply -f mongo-configmap.yaml
- kubectl apply -f mongo-express.yaml
- kubectl get service
- kubectl get pod --watch
- minikube service <servicename>